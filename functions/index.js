const functions = require('firebase-functions')
const fetch = require('node-fetch')
const zlib = require('zlib')

const jsonql = (collection) => {
    const FILTERS = {
        $eq: (tuple, field, value) => tuple[field] === value,
        $gt: (tuple, field, value) => tuple[field] > value,
        $gte: (tuple, field, value) => tuple[field] >= value,
        $lt: (tuple, field, value) => tuple[field] < value,
        $lte: (tuple, field, value) => tuple[field] <= value,
        $ne: (tuple, field, value) => tuple[field] !== value,
        $in: (tuple, field, value) => value.indexOf(tuple[field]) > -1,
        $nin: (tuple, field, value) => value.indexOf(tuple[field]) === -1,
        $and: (tuple, filters) => {
            let condition = true

            filters.forEach((filter) => {
                let [field, clause] = Object.entries(filter)[0]
                let [op, value] = Object.entries(clause)[0]

                condition = condition && FILTERS[op](tuple, field, value)
            })

            return condition
        },
        $or: (tuple, filters) => {
            let condition = false

            filters.forEach((filter) => {
                let [field, clause] = Object.entries(filter)[0]
                let [op, value] = Object.entries(clause)[0]

                condition = condition || FILTERS[op](tuple, field, value)
            })

            return condition
        }
    }

    const select = (collection, arrFields = []) => {
        const filterField = (tuple, arrFields) => {
            if (arrFields.length === 0) {
                return tuple
            } else {
                let obj = {}

                for (i = 0; i < arrFields.length; i++) {
                    let field = arrFields[i]

                    if (field.constructor.name === 'Object') {
                        let [name, fnc] = Object.entries(field)[0]
                        obj[name] = fnc(tuple)
                    } else {
                        obj[field] = tuple[field]
                    }
                }

                return obj
            }
        }

        return collection.map((tuple) => filterField(tuple, arrFields))
    }

    const where = (collection, filters = {}) => {
        let rs = collection.filter((tuple) => {
            let condition = true

            Object.keys(filters).forEach((field) => {
                if (filters[field].constructor.name === 'Array') {
                    condition = condition && FILTERS[field](tuple, filters[field])
                } else {
                    let [op, value] = Object.entries(filters[field])[0]

                    console.log(`op: ${op}\nfield: ${field}\nvalue: ${JSON.stringify(value)}`)
                    condition = condition && FILTERS[op](tuple, field, value)
                }
            })

            return condition
        })

        return {
            select: select.bind(null, rs)
        }
    }

    const join = (dataJoin, attrs) => {
        let rs = []

        collection.forEach((el, idx, arr) => {
            dataJoin.forEach((elj, idxj, arrj) => {
                let match = attrs.reduce((acc, value) => acc &= (el[value[0]] === elj[value[1]]), true)

                if (match) {
                    for (let attrname in elj) {
                        let field = (el[attrname]) ? attrname + '$1' : attrname
                        el[field] = elj[attrname]
                    }
                    rs.push(el)
                }
            })
        })

        return {
            where: where.bind(null, rs),
            select: select.bind(null, rs)
        }
    }

    return {
        join,
        where: where.bind(null, collection),
        select: select.bind(null, collection)
    }
}


exports.helloWorld = functions.https.onRequest((request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE')
    response.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
    response.send(`Hello from Firebase!\n<br/>
        Path: ${request.path}<br/>
        placehold: ${request.path.slice(1)}<br/>
        originalUrl: ${request.originalUrl}<br/>
        body: ${request.body}<br/>
        body.json(): ${JSON.stringify(request.body)}<br/>
        params: ${JSON.stringify(request.params)}<br/>
        `)
})


exports.boardMagrello = functions.https.onRequest(async (request, response) => {
    // const URL_BOARD_MAGRELLO = `https://api.trello.com/1/boards/${request.path.slice(1)}/?actions=all&actions_limit=1000&action_member=false&action_member_fields=false&action_memberCreator=false&lists=all&members=all&cards=visible&card_pluginData=true&key=b539b0d60fe04207d9096f28c9827f83&token=d69619ac92d8a3f7d720e1c799d6739524cfa707a29ffd67501e3027156103a6`;
    const URL_BOARD_LISTS = `https://api.trello.com/1/boards/${request.path.slice(1)}/?lists=all&key=b539b0d60fe04207d9096f28c9827f83&token=d69619ac92d8a3f7d720e1c799d6739524cfa707a29ffd67501e3027156103a6`;
    // const URL_BOARD_CARDS = `https://api.trello.com/1/boards/${request.path.slice(1)}/cards/all?fields=id,closed,dateLastActivity,due,dueComplete,idBoard,idList,name,pluginData&pluginData=true&key=b539b0d60fe04207d9096f28c9827f83&token=d69619ac92d8a3f7d720e1c799d6739524cfa707a29ffd67501e3027156103a6`;
    const URL_BOARD_CARDS = `https://api.trello.com/1/boards/${request.path.slice(1)}/cards/all?fields=checkItemStates,id,closed,dateLastActivity,due,dueComplete,idBoard,idList,name,pluginData&pluginData=true&checkItemStates=true&checklists=all&key=b539b0d60fe04207d9096f28c9827f83&token=d69619ac92d8a3f7d720e1c799d6739524cfa707a29ffd67501e3027156103a6`;
    const URL_BOARD_MEMBERS = `https://api.trello.com/1/boards/${request.path.slice(1)}/members?fields=id,username,fullName&key=b539b0d60fe04207d9096f28c9827f83&token=d69619ac92d8a3f7d720e1c799d6739524cfa707a29ffd67501e3027156103a6`;
    // const URL_BOARD_DATA = `https://api.trello.com/1/boards/${request.path.slice(1)}/?lists=all&members=all&cards=all&card_pluginData=true&key=b539b0d60fe04207d9096f28c9827f83&token=d69619ac92d8a3f7d720e1c799d6739524cfa707a29ffd67501e3027156103a6`;


    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE')
    response.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, accept-encoding, content-encoding")
    response.setHeader('Accept-Encoding', 'gzip, deflate, br')
    response.setHeader('content-encoding', 'gzip')

    try {
        // let res = await fetch(URL_BOARD_MAGRELLO)
        // let json = await res.json()
        // let input = Buffer.from(JSON.stringify(json))

        let input
        let lists = (await (await fetch(URL_BOARD_LISTS)).json()).lists
        let doneList = lists.filter((list) => list.name === 'DONE').pop()

        const URL_ACTIONS_DATA = `https://api.trello.com/1/lists/${doneList.id}/actions/?fields=date,data,type&memberCreator=false&limit=1000&key=b539b0d60fe04207d9096f28c9827f83&token=d69619ac92d8a3f7d720e1c799d6739524cfa707a29ffd67501e3027156103a6`

        let [cards, actions, members] = await Promise.all([
            fetch(URL_BOARD_CARDS).then((response) => response.json()),
            fetch(URL_ACTIONS_DATA).then((response) => response.json()),
            fetch(URL_BOARD_MEMBERS).then((response) => response.json())
        ])

        // console.log(cards)
        // console.log(cards)
        // console.log(members)
        input = Buffer.from(JSON.stringify({ cards, actions, members, lists }))

        response
            .status(200)
            // .json(json)
            .send(zlib.gzipSync(input))
    } catch (e) {
        response
            .status(200)
            .send(`Deu erro!!!\n<br/>${e}`)
    }
})

// app.post("/moveCardToListWebhook", function(request, response) {
// exports.moveCardToListWebhook = functions.https.onRequest((request, response) => {
//     // console.log('Body =>', JSON.stringify(request.body))
//     const idCard = request.body.action.data.card.id
//     const URL_UPDATE_DUE_COMPLETE = `https://api.trello.com/1/cards/${idCard}/dueComplete?value=true&key=b539b0d60fe04207d9096f28c9827f83&token=d69619ac92d8a3f7d720e1c799d6739524cfa707a29ffd67501e3027156103a6`;
//     unirest.put(URL_UPDATE_DUE_COMPLETE)
//         .end((res) => {
//             // console.log(res.body)
//             return true
//         })
//     response.status(200).json({ error: false, msg: 'OK', method: 'POST' })
// })


const registerMoveCardWebhook = () => {
    // https://api.trello.com/1/webhooks/?idModel=54a17d76d4a5072e3931736b&description="My Webhook"&callbackURL=https://mycallbackurl.com
    let idModel = "5c427c6f373f078013adbc3d"
    let description = 'Webhook to register when card was moved to DONE list!'
    // let callbackURL = 'https://magrello.glitch.me/moveCardToListWebhook'
    let callbackURL = 'https://us-central1-magatrello.cloudfunctions.net/moveCardToListWebhook'
    let url = `https://api.trello.com/1/webhooks/5c69e9af4091608d513ecfff?key=b539b0d60fe04207d9096f28c9827f83&token=d69619ac92d8a3f7d720e1c799d6739524cfa707a29ffd67501e3027156103a6`
    // let url = `https://api.trello.com/1/webhooks/?idModel=${idModel}&description="${description}"&callbackURL=${callbackURL}&key=b539b0d60fe04207d9096f28c9827f83&token=d69619ac92d8a3f7d720e1c799d6739524cfa707a29ffd67501e3027156103a6`

    // unirest.delete(url)
    // .end(function (res) {
    //   console.log('data =>', JSON.stringify(res.body))
    // });  
}


// registerMoveCardWebhook();
// https://api.trello.com/1/boards/5c6592ac3d41f6115c105d42/cards/all?fields=checkItemStates,id,closed,dateLastActivity,due,dueComplete,idBoard,idList,name,pluginData&pluginData=true&checklists=all&key=b539b0d60fe04207d9096f28c9827f83&token=d69619ac92d8a3f7d720e1c799d6739524cfa707a29ffd67501e3027156103a6
// https://api.trello.com/1/boards/5c6592ac3d41f6115c105d42/cards/all?fields=checkItemStates,id,closed,dateLastActivity,due,dueComplete,idBoard,idList,name,pluginData&pluginData=true&checkItemStates=true&checklists=all&key=b539b0d60fe04207d9096f28c9827f83&token=d69619ac92d8a3f7d720e1c799d6739524cfa707a29ffd67501e3027156103a6
