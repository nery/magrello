const jsonql = (collection) => {
    const FILTERS = {
        $eq: (tuple, field, value) => tuple[field] === value,
        $gt: (tuple, field, value) => tuple[field] > value,
        $gte: (tuple, field, value) => tuple[field] >= value,
        $lt: (tuple, field, value) => tuple[field] < value,
        $lte: (tuple, field, value) => tuple[field] <= value,
        $ne: (tuple, field, value) => tuple[field] !== value,
        $in: (tuple, field, value) => value.indexOf(tuple[field]) > -1,
        $nin: (tuple, field, value) => value.indexOf(tuple[field]) === -1,
        $and: (tuple, filters) => {
            let condition = true

            filters.forEach((filter) => {
                let [field, clause] = Object.entries(filter)[0]
                let [op, value] = Object.entries(clause)[0]

                condition = condition && FILTERS[op](tuple, field, value)
            })

            return condition
        },
        $or: (tuple, filters) => {
            let condition = false

            filters.forEach((filter) => {
                let [field, clause] = Object.entries(filter)[0]
                let [op, value] = Object.entries(clause)[0]

                condition = condition || FILTERS[op](tuple, field, value)
            })

            return condition
        }
    }

    const select = (collection, arrFields = []) => {
        const filterField = (tuple, arrFields) => {
            if (arrFields.length === 0) {
                return tuple
            } else {
                let obj = {}

                for (i = 0; i < arrFields.length; i++) {
                    let field = arrFields[i]

                    if (field.constructor.name === 'Object') {
                        let [name, fnc] = Object.entries(field)[0]
                        obj[name] = fnc(tuple)
                    } else {
                        obj[field] = tuple[field]
                    }
                }

                return obj
            }
        }

        return collection.map((tuple) => filterField(tuple, arrFields))
    }

    const where = (collection, filters = {}) => {
        let rs = collection.filter((tuple) => {
            let condition = true

            Object.keys(filters).forEach((field) => {
                if (filters[field].constructor.name === 'Array') {
                    condition = condition && FILTERS[field](tuple, filters[field])
                } else {
                    let [op, value] = Object.entries(filters[field])[0]

                    console.log(`op: ${op}\nfield: ${field}\nvalue: ${JSON.stringify(value)}`)
                    condition = condition && FILTERS[op](tuple, field, value)
                }
            })

            return condition
        })

        return {
            select: select.bind(null, rs)
        }
    }

    const join = (dataJoin, attrs) => {
        let rs = []

        collection.forEach((el, idx, arr) => {
            dataJoin.forEach((elj, idxj, arrj) => {
                let match = attrs.reduce((acc, value) => acc &= (el[value[0]] == elj[value[1]]), true)

                if (match) {
                    for (let attrname in elj) {
                        let field = (el[attrname]) ? attrname + '$1' : attrname
                        el[field] = elj[attrname]
                    }
                    rs.push(el)
                }
            })
        })

        return {
            where: where.bind(null, rs),
            select: select.bind(null, rs)
        }
    }

    return {
        join,
        where: where.bind(null, collection),
        select: select.bind(null, collection)
    }
}

// module.export = jsonql
