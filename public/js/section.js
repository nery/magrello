let t = window.TrelloPowerUp.iframe();

// you can access arguments passed to your iframe like so
let arg = t.arg('arg');
let inps = document.querySelectorAll("#magrello INPUT");

function updateMagrelloData(event) {
  event.preventDefault();

  let data = {
    sprintno: window.sprintno.value || '',
    estimativa: window.estimativa.value || 0,
    issue: window.issue.checked
  }

  t.set('card', 'shared', 'magrello', data)
    .then(function() {
      return true
    })
  return true;
}

[...inps].forEach(function(inp) {
  inp.addEventListener('blur', updateMagrelloData);
  inp.onchange = updateMagrelloData;
});


t.render(function() {
  return t.get('card', 'shared', 'magrello')
    .then(function(data) {
      data = data || {}
      window.sprintno.value = data.sprintno || '';
      window.estimativa.value = data.estimativa || 0;
      window.issue.checked = data.issue;
    })
    .then(function() {
      t.sizeTo('#magrello').done();
    });
});