var t = window.TrelloPowerUp.iframe()

const PLUGIN_NAME = 'SoSimple';// Magrello ;
const PLUGIN = {
  Magrello: {
    ID: '5c429849b70482100770cd7a', // Magrello
    URL: 'https://magrello.glitch.me' // Magrello  
  },
  SoSimple: {
    ID: '5c429849b70482100770cd7a', // Magrello
    // ID: '5c6e899f1e4387693061d630', // SoSimple
    URL: 'https://us-central1-magatrello.cloudfunctions.net' // SoSimple
    // URL: 'http://localhost:5001/magatrello/us-central1'
  }
}[PLUGIN_NAME]

const jsonql = (collection) => {
  const FILTERS = {
    $eq: (tuple, field, value) => tuple[field] === value,
    $gt: (tuple, field, value) => tuple[field] > value,
    $gte: (tuple, field, value) => tuple[field] >= value,
    $lt: (tuple, field, value) => tuple[field] < value,
    $lte: (tuple, field, value) => tuple[field] <= value,
    $ne: (tuple, field, value) => tuple[field] !== value,
    $in: (tuple, field, value) => value.indexOf(tuple[field]) > -1,
    $nin: (tuple, field, value) => value.indexOf(tuple[field]) === -1,
    $and: (tuple, filters) => {
      let condition = true

      filters.forEach((filter) => {
        let [field, clause] = Object.entries(filter)[0]
        let [op, value] = Object.entries(clause)[0]

        condition = condition && FILTERS[op](tuple, field, value)
      })

      return condition
    },
    $or: (tuple, filters) => {
      let condition = false

      filters.forEach((filter) => {
        let [field, clause] = Object.entries(filter)[0]
        let [op, value] = Object.entries(clause)[0]

        condition = condition || FILTERS[op](tuple, field, value)
      })

      return condition
    }
  }

  const select = (collection, arrFields = []) => {
    const filterField = (tuple, arrFields) => {
      if (arrFields.length === 0) {
        return tuple
      } else {
        let obj = {}

        for (i = 0; i < arrFields.length; i++) {
          let field = arrFields[i]

          if (field.constructor.name === 'Object') {
            let [name, fnc] = Object.entries(field)[0]
            obj[name] = fnc(tuple)
          } else {
            obj[field] = tuple[field]
          }
        }

        return obj
      }
    }

    return collection.map((tuple) => filterField(tuple, arrFields))
  }

  const where = (collection, filters = {}) => {
    let rs = collection.filter((tuple) => {
      let condition = true

      Object.keys(filters).forEach((field) => {
        if (filters[field].constructor.name === 'Array') {
          condition = condition && FILTERS[field](tuple, filters[field])
        } else {
          let [op, value] = Object.entries(filters[field])[0]

          console.log(`op: ${op}\nfield: ${field}\nvalue: ${JSON.stringify(value)}`)
          condition = condition && FILTERS[op](tuple, field, value)
        }
      })

      return condition
    })

    return {
      select: select.bind(null, rs)
    }
  }

  const join = (dataJoin, attrs) => {
    let rs = []

    collection.forEach((el, idx, arr) => {
      dataJoin.forEach((elj, idxj, arrj) => {
        let match = attrs.reduce((acc, value) => acc &= (el[value[0]] == elj[value[1]]), true)

        if (match) {
          for (let attrname in elj) {
            let field = (el[attrname]) ? attrname + '$1' : attrname
            el[field] = elj[attrname]
          }
          rs.push(el)
        }
      })
    })

    return {
      where: where.bind(null, rs),
      select: select.bind(null, rs)
    }
  }

  return {
    join,
    where: where.bind(null, collection),
    select: select.bind(null, collection)
  }
}

const id$ = (id) => document.getElementById(id)

let tirbeOptions = ['CREED', 'Ecommerce', 'PDV Ponto Físico']

let squadOptions = ['Faturamento 3P', 'Johnny', 'MaaS API', 'PDV Ponto Físico', 'Recebimento Fiscal 3P']


function runFncUsingMagrelloData(fncToRun) {
  const boardId = restoreItem('boardId')
  const URL_BOARD_DATA = `${PLUGIN.URL}/boardMagrello/${boardId}`
  let di = new Date().getTime()

  document.body.classList.add('loading')

  fetch(URL_BOARD_DATA, { method: 'GET' })
    .then(function(resp) { return resp.json() })
    .then(function(data) {
      // let d1 = new Date().getTime()
      document.body.classList.remove('loading')
      fncToRun(extractMagrelloData(data))
      // let d2 = new Date().getTime()
      // console.log(`### TIME ###################\nfetch: ${d1 - di} ms\nupdateChart: ${d2 - d1} ms`)
    })
}

const calculeLastDate = (pbis) => {
  let today = new Date().toISOString().slice(0, 10)

  let lastPBIsDate = pbis.reduce((max, pbi) => {
    if (pbi.status !== 'DONE' && pbi.status !== 'PRODUCTION' && !pbi.status.startsWith('SPRINT #')) {
      return today
    } else {
      return [max, pbi.dueDate, pbi.movimentationDate].reduce((max, curr) => (curr > max) ? curr : max)
    }
    // return (!pbi.dueDate === undefined || max > pbi.dueDate) ? max : pbi.dueDate
  }, '')

  return new Date(lastPBIsDate)
}

const fmtDate = (date) => {
  return date.toISOString().substring(5, 10).split('-').reverse().join('/')
}

const calculeWeekendDays = (initialDate, finishDate) => {
  const dayMilliseconds = 1000 * 60 * 60 * 24
  let currentDate = initialDate
  let weekendDays = 0

  while (currentDate <= finishDate) {
    let day = currentDate.getDay()

    if (day === 0 || day === 6) {
      weekendDays++
    }
    currentDate = new Date(+currentDate + dayMilliseconds)
  }

  return weekendDays
}

function extractChartData(sprint) {
  const today = new Date()
  const maxDate = calculeLastDate(sprint.pbis)
  const ds = new Date(sprint.start)
  const df = new Date(sprint.finish)
  const lastDayWorked = maxDate.getTime() > today.getTime() ? today : maxDate
  const weekendDays = calculeWeekendDays(ds, df)
  const weekendDaysUntilLastWorkedDay = calculeWeekendDays(ds, lastDayWorked)
  let daysPlanned = Math.round((df.getTime() - ds.getTime()) / 1000 / 60 / 60 / 24) + 1 - weekendDays
  let daysWorked = Math.round((lastDayWorked.getTime() - ds.getTime()) / 1000 / 60 / 60 / 24) - weekendDaysUntilLastWorkedDay
  let speedPlanned = (sprint.totalHours / daysPlanned) | 0
  let speedExecuted = (sprint.doneHours / (daysWorked === 0 ? 1 : daysWorked)) | 0

  // console.log('lastDayWorked:', lastDayWorked.toISOString().slice(0, 10), '\nstart.:', ds.toISOString().slice(0, 10), '\nfinish:', df.toISOString().slice(0, 10))
  // console.log({ lastDayWorked, daysPlanned, daysWorked, speedPlanned, speedExecuted, weekendDays, sprint })

  speedPlanned = (speedPlanned <= 1) ? 8 : speedPlanned
  speedExecuted = (isNaN(speedExecuted) || speedExecuted <= 1) ? Math.round(speedPlanned / 2) : speedExecuted

  let xLabels = []
  let planned = []
  let executed = []
  let dx = new Date(ds)
  let htx = sprint.totalHours
  let hdx = sprint.totalHours

  xLabels.push(fmtDate(dx))
  planned.push(htx)
  executed.push(hdx)

  while (htx > 0 || hdx > 0) {
    dx.setDate(dx.getDate() + 1)
    xLabels.push(fmtDate(dx))
    if (dx.getDay() !== 0 && dx.getDay() !== 6) {
      htx -= speedPlanned
      hdx -= speedExecuted
    }
    planned.push((htx < 0) ? 0 : htx)
    executed.push((hdx < 0) ? 0 : hdx)
  }

  dx.setDate(dx.getDate() + 1)
  xLabels.push(fmtDate(dx))
  planned.push(0)
  executed.push(0)

  return { labels: xLabels, planned, executed, speedPlanned, speedExecuted }
}

function createChart(sprint) {
  // let { labels, planned, executed } = extractChartData(sprint, actions)
  let { labels, planned, executed } = (sprint)
    ? extractChartData(sprint)
    : { labels: [], planned: [0, 0], executed: [0, 0] }

  const ctx = id$("burndownChart");

  let burndownChart = new Chart(ctx, {
    type: "line",
    data: {
      labels: labels,
      datasets: [
        {
          data: planned,
          label: "Planejado",
          borderColor: "#2196F3",
          fill: false
        },
        {
          data: executed,
          label: "Executado",
          borderColor: "#f5994e",
          fill: false
        }
      ]
    },
    options: {
      maintainAspectRatio: false
    }
  });

  return burndownChart
}

function storeItem(key, value) {
  if (typeof (Storage) !== "undefined") {
    sessionStorage.setItem(key, JSON.stringify(value))
  }
}

function restoreItem(key) {
  return JSON.parse(sessionStorage.getItem(key))
}

function storeSession() {
  const sessionItems = {
    squadLeader: id$("selectSquadLeader").value
  }

  storeItem("sessionItems", sessionItems)
}

function restoreSession() {
  if (typeof (Storage) !== "undefined") {
    const items = restoreItem("sessionItems");

    if (items) {
      id$("selectSquadLeader").value = items.squadLeader;
    }
  }
}

function createSelectOptions(options, select) {
  const selector = id$(select)

  for (let option of options) {
    const opt = document.createElement("OPTION");
    opt.setAttribute("value", option.toLocaleLowerCase());
    const t = document.createTextNode(option);
    opt.appendChild(t);
    selector.appendChild(opt);
  }
}

function createTableRows(pbis = [], tableBodyID) {
  const tableRow = id$(tableBodyID)
  const headers = ['name', 'hours', 'dueDate', 'movimentationDate', 'status']
  const extractDate = (date) => (date) ? date.slice(0, 10) : ''

  tableRow.innerText = ''

  for (let item of pbis) {
    const tr = document.createElement("TR");

    for (let header of headers) {
      const td = document.createElement("TD");
      // const t = document.createTextNode(item[header]);

      td.appendChild(document.createTextNode((header === 'dueDate' || header === 'movimentationDate')
        ? extractDate(item[header])
        : item[header]));
      tr.appendChild(td);

      if (header === 'hours') {
        td.style.textAlign = 'right'
        td.style.paddingRight = '16px';
      } else if (header === 'status' && item.late) {
        td.classList.add('text-red')
      } else if (header === 'movimentDate' && item.late) {
        td.classList.add('text-red')
      } else if (header === 'dueDate' && item.dueDate === '') {
        td.classList.add('text-red')
        td.removeChild(td.childNodes[0])
        td.appendChild(document.createTextNode('SEM DATA'))
        td.style.cssText = 'color: #b30000; font-weight: bold;'
      }
    }

    tableRow.appendChild(tr);
  }
}

function updateChart({ members, sprints, actions }) {
  let burndownChart = this.burndownChart
  let sprint = sprints[this.sprintId]
  let { labels, planned, executed, speedPlanned, speedExecuted } = (sprint)
    ? extractChartData(sprint, actions)
    : { labels: [], planned: [0, 0], executed: [0, 0], speedPlanned: 0, speedExecuted: 0 }
  // alert(JSON.stringify({labels, planned, executed}))

  id$('speedplan').value = speedPlanned
  id$('speedexec').value = speedExecuted

  burndownChart.data.labels = labels;
  burndownChart.data.datasets.pop();
  burndownChart.data.datasets.pop();
  burndownChart.data.datasets.push({
    data: planned,
    label: "Planejado",
    borderColor: "#2196F3",
    fill: false
  })
  burndownChart.data.datasets.push({
    data: executed,
    label: "Executado",
    borderColor: "#f5994e",
    fill: false
  })
  burndownChart.update()
}

function updateSprintData(selectedSprint) {
  let sprint = (selectedSprint === undefined)
    ? { totalHours: 0, doneHours: 0, issueHours: 0, pbis: [] }
    : selectedSprint

  id$('totalHours').value = sprint.totalHours
  id$('doneHours').value = sprint.doneHours
  id$('issueHours').value = sprint.issueHours
  createTableRows(sprint.pbis, "tableBody")
}

const compareMinDates = (currMinDate, date, estimativa) => {
  const dayMilliseconds = 24 * 60 * 60 * 1000
  let minDate = new Date(currMinDate)
  let newDate = new Date(`${(date || '2099-12-31').substring(0, 10)}T03:00:00.000Z`)
  let delta = ((((estimativa / 8) | 0) < 1) ? 1 : (estimativa / 8) | 0) - 1

  newDate.setTime(newDate.getTime() - (delta * dayMilliseconds))
  return (minDate < newDate)
    ? minDate.toISOString()
    : newDate.toISOString()
}

const compareMaxDates = (currMinDate, date, estimativa) => {
  // "2019-02-19T15:00:00.000Z" => "T03:00:00.000Z"
  let minDate = new Date(currMinDate)
  let newDate = new Date(`${(date || '2019-01-01').substring(0, 10)}T03:00:00.000Z`)

  // newDate.setTime(newDate.getTime() - (Math.trunc(estimativa/8)*24*60*60*1000))
  return (minDate > newDate)
    ? minDate.toISOString()
    : newDate.toISOString()
}

const calculePartiallyDoneHours = (hours, checklists) => {
  const checkItems = (checklists[0] || {checkItems: []}).checkItems
  const hoursForItem = (checkItems.length === 0) ? 0 : hours / checkItems.length
  const finishedItems = checkItems.reduce((acc, item) => (item.state === "complete") ? ++acc : acc, 0)
  // console.log({hours, checkItems: checkItems.length, hoursForItem, finishedItems})

  return (finishedItems * hoursForItem) | 0
}

const reduceSprintsFnc = (acc, { id, name, name$1, due, dueComplete, idMembers, checklists = [], pluginData = [] }) => {
  // const filterPlugin = (data) => data.idPlugin === PLUGIN.ID
  const filterPlugin = (data) => data.value && data.value.startsWith('{"magrello":')
  const isDone = (dueComplete, listName) => dueComplete || listName === 'DONE' || listName === 'PRODUCTION'
  const getData = (data) => JSON.parse((data || { value: '{}' }).value)
  let mag = getData(pluginData.filter(filterPlugin)[0]).magrello

  if (mag === undefined) {
    mag = { sprintno: '', estimativa: 0 }
  }
  //   return acc
  // } else {
  let qdeMembros = (idMembers && idMembers.length > 0) ? idMembers.length : 1
  let horas = parseInt(mag.estimativa || 0)
  let sprintCard = acc[mag.sprintno] || {
    pbis: [],
    totalHours: 0,
    doneHours: 0,
    partiallyDoneHours: 0,
    issueHours: 0,
    start: "2099-12-31T03:00:00.000Z",
    finish: "2019-01-01T03:00:00.000Z"
  }
  let partiallyDoneHours = isDone(dueComplete, name$1) ? 0 : calculePartiallyDoneHours(horas, checklists)

  sprintCard.pbis.push({
    id,
    name,
    hours: mag.estimativa || 0,
    // dueDate: (due || '').substring(0,10),
    dueDate: due || '',
    dueComplete: dueComplete,
    status: name$1
  })
  sprintCard.totalHours += horas
  sprintCard.partiallyDoneHours += partiallyDoneHours
  sprintCard.doneHours += isDone(dueComplete, name$1) ? horas : partiallyDoneHours
  sprintCard.issueHours += (mag.issue) ? horas : 0
  sprintCard.start = compareMinDates(sprintCard.start, due, horas / qdeMembros)
  sprintCard.finish = compareMaxDates(sprintCard.finish, due)
  acc[mag.sprintno] = sprintCard

  return acc
  // }
}

const reduceActionFnc = (actCardArr, action) => {
  let { data, type, date } = action

  if (data.listAfter && data.card && data.listAfter.name === 'DONE') {
    actCardArr.push({ ...data.card, type, date })
  }
  return actCardArr
}

function classifySprint(actions, sprint) {
  sprint.pbis.forEach((pbi) => {
    let found = actions.find((action, idx) => pbi.id === action.id)
    let dueDate = new Date(pbi.dueDate)
    let movDate

    if (found) {
      pbi.movimentationDate = found.date
      movDate = new Date(pbi.movimentationDate)
      movDate.setHours(0, 0, 0)
      dueDate.setHours(23, 59, 59)
      pbi.late = (movDate > dueDate) ? true : false
    } else {
      let today = new Date()

      pbi.movimentationDate = undefined
      today.setHours(23, 59, 59)
      dueDate.setHours(0, 0, 0)
      pbi.late = (today > dueDate) ? true : false
    }
  })
  sprint.pbis.sort((a, b) => (a.dueDate > b.dueDate) - (a.dueDate < b.dueDate))
}

const extractMagrelloData = ({ actions, cards, members, lists }) => {
  let membersAjusted = members.map((memb) => '@' + memb.username)
  let fullCards = jsonql(cards)
    .join(lists, [['idList', 'id']])
    .select(['id', 'name', 'name$1', 'due', 'dueComplete', 'pluginData', 'id$1', 'idMembers', 'checklists'])
  let sprints = fullCards.reduce(reduceSprintsFnc, {})
  let actionsFiltred = actions.reduce(reduceActionFnc, [])

  // console.log('ACTIONS:', actions.length)
  Object.values(sprints).forEach(classifySprint.bind(null, actionsFiltred))

  return {
    sprints,
    members: membersAjusted,
    actions: actionsFiltred,
    cards: cards/*,
    lists: data.lists */
  }

}

const savePerformanceInfo = () => {
  const info = {
    squadLeader: id$("selectSquadLeader").value,
    totalHours: id$("inpTotalHours").value,
    plannedHours: id$("inpPlannedHours").value,
    executedHours: id$("inpExecutedHours").value,
    executedPercent: id$("inpExecutedPercent").value
  }

  if (window.location.host === 'localhost:5000') {
    console.log(info)
    storeItem("performanceInfo", info)
  } else {
    t.set('board', 'shared', 'performanceInfo', JSON.stringify(info))
      .then(function() {
        console.log('savePerformanceInfo =>\n', info)
        return true
      })
  }
}

const updatePerformanceInfo = (sprints) => {
  const updateHtmlFields = (perfInfo) => {
    id$("selectSquadLeader").value = perfInfo.squadLeader
    id$("inpTotalHours").value = perfInfo.totalHours
    id$("inpPlannedHours").value = perfInfo.plannedHours
    id$("inpExecutedHours").value = '' + perfInfo.executedHours
    id$("inpExecutedPercent").value = '' + perfInfo.executedPercent
  }

  if (window.location.host === 'localhost:5000') {
    updateHtmlFields(restoreItem("performanceInfo") || {})
  } else {
    t.get('board', 'shared', 'performanceInfo')
      .then(data => {
        const info = JSON.parse(data || '{}')
        const perf = calculePerformanceInfo(sprints)

        perf.squadLeader = info.squadLeader
        perf.totalHours = info.totalHours
        perf.executedPercent = (perf.executedHours / perf.totalHours * 100) | 0

        updateHtmlFields(perf)
        storeItem("performanceInfo", perf)
      })
      .catch(error => {
        updateHtmlFields(restoreItem("performanceInfo") || {})
      })
  }
}

const calculePerformanceInfo = (sprints) => {
  const plannedHours = Object.values(sprints).reduce((horas, sprint) => horas + sprint.totalHours, 0)
  const executedHours = Object.values(sprints).reduce((horas, sprint) => horas + sprint.doneHours, 0)

  return {
    plannedHours,
    executedHours
  }
}

function init({ members, sprints, cards, actions }) {
  // console.log('-------------------\nmembers: ', members)
  // console.log('actions: ', actions)
  console.log('sprints: ', sprints)
  console.log('cards: ', cards)
  let sprint = sprints['']; // || { pbis: [], totalHours: 0, doneHours: 0 }
  let burndownChart = createChart(sprint)

  createSelectOptions(members, "selectSquadLeader")
  // restoreSession("sessionItems")
  updatePerformanceInfo(sprints)
  updateSprintData(sprint)
  id$('burndownTitle').click()
  id$('btnSalvar').addEventListener('click', (event) => {
    event.preventDefault()
    savePerformanceInfo()
  })

  id$('inpSprint').addEventListener('change', (evt) => {
    // let d1, d2, di = new Date().getTime()
    updateSprintData(sprints[evt.target.value])
    // d1 = new Date().getTime()
    id$('sprintno').value = evt.target.value
    // runFncUsingMagrelloData(updateChart.bind({ burndownChart, sprintId: evt.target.value }))
    updateChart.call({ burndownChart, sprintId: evt.target.value }, { members, sprints, actions })
    // d2 = new Date().getTime()
    // console.log(`updateSprintData: ${d1 - di} ms\nupdateChart: ${d2 - d1} ms`)
  })

  id$('sprintno').addEventListener('change', (evt) => {
    // let d1, d2, di = new Date().getTime()
    runFncUsingMagrelloData(updateChart.bind({ burndownChart, sprintId: evt.target.value }))
    // d1 = new Date().getTime()
    id$('inpSprint').value = evt.target.value
    updateSprintData(sprints[evt.target.value])
    // d2 = new Date().getTime()
    // console.log(`updateChart: ${d1 - di} ms\nupdateSprintData: ${d2 - d1} ms`)
  })

  t.render(function() {
    // this function we be called once on initial load and then called each 
    // time something changes that you might want to react to, such as new 
    // data being stored with t.set()
  })
}

function main(location) {
  let context = JSON.parse(unescape(location).replace(/^http.*?\.html\?*\#/, '')).context

  console.log(`${PLUGIN_NAME}(BoardId): `, context.board)
  storeItem('boardId', context.board)

  // fetch(`https://magrello.glitch.me/boardMagrello/${context.board}`, { method: 'GET' })
  // fetch(`https://us-central1-magatrello.cloudfunctions.net/boardMagrello/${context.board}`, { method: 'GET' })
  fetch(`${PLUGIN.URL}/boardMagrello/${context.board}`, {
    // fetch('/test.json', {
    method: 'GET',
    mode: 'cors',
    headers: new Headers({
      'content-encoding': 'gzip',
      'accept-encoding': 'gzip, deflate, br'
    })
  })
    .then(function(resp) { return resp.json() })
    .then(function(dataBoard) {
      // console.log('==>\n', dataBoard)
      init(extractMagrelloData(dataBoard))
    })
    .catch(function(err) {
      console.log('Failed retrieving information', err);
    })
}
// stepZero(window.location.href)

//==============================================================================

const test = async () => {
  // const boardId = '5c6592ac3d41f6115c105d42' // mglu3-creed
  const boardId = '5c42776e7f6c573d6af9457e' // MagaTest
  const URL_LISTS_DATA = `https://api.trello.com/1/boards/${boardId}/?lists=all&key=b539b0d60fe04207d9096f28c9827f83&token=d69619ac92d8a3f7d720e1c799d6739524cfa707a29ffd67501e3027156103a6`

  let lists = (await (await fetch(URL_LISTS_DATA)).json()).lists
  let doneList = lists.filter((list) => list.name === 'DONE').pop()
  let URL_ACTIONS_DATA = `https://api.trello.com/1/lists/${doneList.id}/actions/?fields=date,data,type&memberCreator=false&limit=1000&key=b539b0d60fe04207d9096f28c9827f83&token=d69619ac92d8a3f7d720e1c799d6739524cfa707a29ffd67501e3027156103a6`

  let actionsData = await fetch(URL_ACTIONS_DATA).then((response) => response.json())
}

main(window.location.href)


// MagaTest: 5c42776e7f6c573d6af9457e
// TesteMoverQuadro: 5c9b65b9cb1c066fdec1e748
// mglu3-creed: 5c6592ac3d41f6115c105d42
// mglu3-portal-de-conciliação: 5ca247cd33df26738c4c0133
// mglu3-pdv-seller: 5c93fa70416d9344ee234685
// mglu3-faturamento: 5c915a39448ec278406a0e69
// MGLU3 - Qualidade: 5c9bc3aeb5a1a242e50a1227
// PDV Ponto Fisico: 5c66c2603def54799305f890 / 5c66c2603def54799305f890
// Creed - Recebimento & Parametrizações 3P: 5c6596f04248dc7523d84351
// Creed - Qualidade: 5c7823b5979587196d60917c
// Creed - SAC: 5c6ed5b0509b4672539af5f1
// E-commerce - Johnnie: 5c66fa09943b4645502e4eb3
// E-commerce - Kelex: 5c76a4c34d76887142c3bd36
// E-commerce - Maas API Plataform: 5c66fa6ce1ab22300c762141
// http://localhost:5000/board-bar.html#{"context":{"board":"5c93fa70416d9344ee234685"}}
// https://magatrello.firebaseapp.com/board-bar.html#{"context":{"board":"5c6592ac3d41f6115c105d42"}}


