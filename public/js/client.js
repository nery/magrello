const MAGRELLO_IFRAME_CONNECTOR = 'https://magrello.glitch.me/index'
const SOSIMPLE_IFRAME_CONNECTOR = 'https://magatrello.firebaseapp.com/index.html'

const MAGRELLO_WHITE_ICON = "https://magatrello.firebaseapp.com/img/magrello_white_icon.png";
const MAGRELLO_GRAY_ICON = "https://magatrello.firebaseapp.com/img/magrello_gray_icon.png";
const CALENDAR_ICON = "https://magatrello.firebaseapp.com/img/calendar.svg";
// const MAGRELLO_WHITE_ICON = "https://cdn.glitch.com/675ae6e6-233b-405b-8e55-2b9d85ff0808%2Fmagrello_white.png?1548016020549";
// const MAGRELLO_GRAY_ICON = "https://cdn.glitch.com/675ae6e6-233b-405b-8e55-2b9d85ff0808%2Fmagrello_gray.png?1548015871975";
// const CALENDAR_ICON = "https://cdn.glitch.com/675ae6e6-233b-405b-8e55-2b9d85ff0808%2Fcalendar.svg?1548015033179";


const boardButtonCallback = function(t, opts) {
	return t.boardBar({
		url: './board-bar.html',
		height: 700
	})
		.then(function() {
			// return t.closePopup();
			return true;
		});
}

window.TrelloPowerUp.initialize({

	'board-buttons': function(t, options) {
		return [{
			// we can either provide a button that has a callback function
			// that callback function should probably open a popup, overlay, or boardBar
			icon: MAGRELLO_WHITE_ICON,
			text: 'Magrello',
			callback: boardButtonCallback
		}/*, {
			// or we can also have a button that is just a simple url
			// clicking it will open a new tab at the provided url
			icon: WHITE_ICON,
			text: 'URL',
			url: 'https://trello.com/inspiration',
			target: 'Inspiring Boards' // optional target for above url
		}*/];
	},
	/*
	  'card-buttons': function (ctx, options) {
		  return [{
			  icon: ESTIMATIVA_ICON,
			  text: 'Estimativa (h)',
			  callback: function (ctx) {
				  return ctx.popup({
					  title: "Estimativa (h)",
					  url: 'estimate.html'
				  });
			  }
		  }];
	  },
	*/
	'card-badges': function(t, options) {
		// t.alert({ message: 'Powering-Up, give xxxxxxxxx...' });
		return t.get('card', 'shared', 'magrello')
			.then(function(data) {
				let text = (data && data.estimativa)
					? `# ${data.sprintno} | ${data.estimativa} hs`
					: '# ? | ? hs'

				return [{
					icon: CALENDAR_ICON,
					text: text,
					color: (data && data.estimativa && data.sprintno && data.sprintno !== '') ? 'sky' : 'red',
				}];
			});
	},

	'card-back-section': function(t, options) {
		// t.alert({ message: 'card-back-section...' });

		return {
			icon: MAGRELLO_GRAY_ICON, // Must be a gray icon, colored icons not allowed.
			title: 'Magrello',
			content: {
				type: 'iframe',
				url: t.signUrl('./section.html', {
					arg: 'you can pass your section args here'
				}),
				height: 120
			}
		};
	}

});


// fetch('https://magrello.glitch.me/k3t0', {method: 'POST'})
//   .then(function(resp) { return resp.json() })
//   .then(function(data) { 
//     sessionStorage.setItem('k3t0', JSON.stringify(data))
//   })

